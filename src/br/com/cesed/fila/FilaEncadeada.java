package br.com.cesed.fila;
/***
 * Implementacao da estruturada de dados pilha encadeada
 * @author tessa
 *
 */
public class FilaEncadeada {
		
	
	private No inicio;
	private No fim;
	
	private int tamanho;
	
	/***
	 * 
	 */
	public FilaEncadeada() {
		tamanho = 0;
		inicio = null;
		fim = null;
	}
	/***
	 * Metodo responsavel por criar uma fila caso n�o exista nenhuma fila implementada ainda
	 * @param elemento
	 */
	public void criarFila(Object elemento) {
		if(isEmpty()) {
			No novo = new No(elemento);
			inicio = novo;
			fim = novo;
			tamanho++;
		}
	}
	/***
	 * Metodo responsavel por inserir o elemento no final da fila
	 * @param elemento
	 */
	public void enqueue(Object elemento) {
		No novo = new No(elemento);
		if(size() == 0) {
			criarFila(elemento);
		}else {
			fim.setProximo(novo);
			fim = novo;
			tamanho++;	
		}
	}
	/***
	 * Metodo responsavel por retirar o primeiro elemento da fila, e retornar esse elemento para visualiza��o
	 * @return primeiro elemento da fila
	 */
	public Object dequeue() {
		Object elemento;
		if(isEmpty()) {
			throw new IllegalArgumentException("Fila n�o existe");
		}else if(inicio == fim) {
			elemento = inicio.getValor();
			inicio = null;
			tamanho--;
		}else {
			No aux = inicio;
			elemento = inicio.getValor();
			aux = inicio.getProximo();
			inicio = aux;
			tamanho--;
			
		}
	
		return elemento;	
		
	}
	/***
	 * Metodo responsavel por retornar o primeiro elemento da fila sem excluir o mesmo
	 * @return primeiro elemento da fila
	 */
	public Object front() {
		if(isEmpty()) {
			throw new NullPointerException("Valor n�o existe");
		}
	
		return inicio.getValor();
	}
	/***
	 * 	Metodo responsavel por fornecer o tamanho da fila
	 * @return tamanho
	 */
	public int size() {

		return tamanho;
	}
	/***
	 * 	Metodo responsavel por afirmar se a fila esta vazia ou n�o
	 * @return true ou false
	 */
	public boolean isEmpty() {
		return tamanho == 0;
	}

	public No getInicio() {
		return inicio;
	}

	public void setInicio(No inicio) {
		this.inicio = inicio;
	}

	public No getFim() {
		return fim;
	}

	public void setFim(No fim) {
		this.fim = fim;
	}

	public int getTamanho() {
		return tamanho;
	}

	public void setTamanho(int tamanho) {
		this.tamanho = tamanho;
	}

}
