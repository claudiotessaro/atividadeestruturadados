package br.com.cesed.pilha;
/***
 * Implementa��o de uma Pilha encadeada
 * @author Claudio Tessaro
 *
 */
public class PilhaEncadeada {
	
	No topo;
	
	int tamanho;
	
/***
 * Metodo que insere um elemento no topo da lista	
 * @param elemento
 */
	public void push(Object elemento) {
		No novo = new No(elemento);
		novo.setAnterior(topo);
		topo = novo;
		tamanho++;
		
	}
	
/***
 * Metodo que elimina o elemento e retorna o elemento que foi excluido	
 * @return elemento
 */
	public Object pop() {
		if(isEmpty()) {
			throw new IllegalArgumentException("Pilha esta vazia");
		}else {
			Object elemento = topo.getElemento();
			topo = topo.getAnterior();
			tamanho--;
			return elemento;
		}
	}
/***
 * Metodo que retorna qual o elemento do topo
 * @return elementoTopo
 */
	public Object top() {
		if(tamanho <=0) {
			throw new NullPointerException("N�o existe valor");
		}
		return topo.getElemento();
	}
/***
 * Metodo que retorna o tamanho da lista	
 * @return tamanho
 */
	public int size() {
		return tamanho;
	}
/***
 * Metodo que retorna se a lista � vazia ou n�o			
 * @return true
 */
	public boolean isEmpty() {
		return tamanho == 0;
	}

}
