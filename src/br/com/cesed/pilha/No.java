package br.com.cesed.pilha;

public class No {
	
	private No anterior;
	private Object elemento;
	
	public No(Object elemento) {
		this.elemento = elemento;
		this.anterior = null;
	}
	
	
	public No getAnterior() {
		return anterior;
	}
	public void setAnterior(No anterior) {
		this.anterior = anterior;
	}
	public Object getElemento() {
		return elemento;
	}
	public void setElemento(Object elemento) {
		this.elemento = elemento;
	}
	
	

}
