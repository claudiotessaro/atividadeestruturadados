package br.com.cesed.teste;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import br.com.cesed.lista.ListaDE;

class ListaTeste {

	@Test
	void testCriarLista() {
		ListaDE lista = new ListaDE();
		assertTrue(lista.isEmpty());
		assertEquals(0, lista.size());
		lista.criarLista(10);
		assertFalse(lista.isEmpty());
		assertEquals(1, lista.size());
	}

	@Test
	void testAddInicio() {
		ListaDE lista = new ListaDE();
		assertEquals(0, lista.size());
		assertTrue(lista.isEmpty());
		lista.addInicio("teste");
		assertEquals("teste", lista.getComeco());
		lista.addInicio("leao");
		assertEquals("leao", lista.getComeco());
		assertEquals("teste", lista.getFinal());
		lista.remove(1);
		assertEquals(1, lista.size());
		lista.remove(0);
		assertEquals(0, lista.size());
	}

	@Test
	void testAddFinal() {
		ListaDE lista = new ListaDE();
		assertEquals(0, lista.size());
		assertTrue(lista.isEmpty());
		lista.addFinal("teste");
		assertEquals("teste", lista.getFinal());
		lista.addFinal("girafa");
		assertEquals("girafa",lista.getFinal());
	}

	@Test
	void testRemoveInicio() {
		ListaDE lista = new ListaDE();
		assertEquals(0, lista.size());
		assertTrue(lista.isEmpty());
		lista.addInicio(1);
		lista.addInicio(2);
		lista.removeInicio();
		assertEquals(1, lista.size());

	}

	@Test
	void testRemoveFim() {
		ListaDE lista = new ListaDE();
		assertEquals(0, lista.size());
		assertTrue(lista.isEmpty());
		lista.addInicio(1);
		lista.addInicio(2);
		lista.removeFim();
		assertEquals(1, lista.size());
		lista.removeFim();
		assertEquals(0,lista.size());
	}

	@Test
	void testRemove() {
		ListaDE lista = new ListaDE();
		assertEquals(0, lista.size());
		assertTrue(lista.isEmpty());
		lista.addInicio(1);
		lista.addInicio(2);
		lista.remove(1);
		assertEquals(1, lista.size());
		lista.remove(0);
		assertEquals(0, lista.size());
	}

	@Test
	void testSize() {
		ListaDE lista = new ListaDE();
		assertEquals(0, lista.size());
		assertTrue(lista.isEmpty());
		lista.addInicio(1);
		assertEquals(1,lista.size());
	}

	@Test
	void testIsEmpty() {
		ListaDE lista = new ListaDE();
		assertEquals(0, lista.size());
		assertTrue(lista.isEmpty());
		lista.addInicio(1);
		assertFalse(lista.isEmpty());
	}

}
