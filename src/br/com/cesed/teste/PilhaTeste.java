package br.com.cesed.teste;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import br.com.cesed.pilha.PilhaEncadeada;

class PilhaTeste {

	@Test
	void testPush() {
		PilhaEncadeada pilha = new PilhaEncadeada();
		assertEquals(0, pilha.size());
		pilha.push(2);
		assertEquals(1, pilha.size());
		pilha.push(3);
		assertEquals(2, pilha.size());
		
	}

	@Test
	void testPop() {
		PilhaEncadeada pilha = new PilhaEncadeada();
		assertEquals(0, pilha.size());
		pilha.push("maria");
		assertEquals(1, pilha.size());
		pilha.push("joao");
		pilha.push("elemento");
		assertEquals(3, pilha.size());
		assertEquals("elemento",pilha.pop());
		assertEquals(2,pilha.size());
		assertEquals("joao",pilha.pop());
		assertEquals(1,pilha.size());
		assertEquals("maria",pilha.pop());
		assertEquals(0,pilha.size());
		assertThrows(IllegalArgumentException.class, ()->{
			pilha.pop();
		});
	}

	@Test
	void testTop() {
		PilhaEncadeada pilha = new PilhaEncadeada();
		assertThrows(NullPointerException.class, ()->{
			pilha.top();
		});
		
		assertEquals(0, pilha.size());
		pilha.push("1");
		assertEquals(1, pilha.size());
		assertEquals("1", pilha.top());
		pilha.push("2");
		assertEquals("2", pilha.top());
		pilha.push("3");
		assertEquals("3", pilha.top());
	}

	@Test
	void testSize() {
		PilhaEncadeada pilha = new PilhaEncadeada();
		assertEquals(0, pilha.size());
		pilha.push(20);
		assertEquals(1, pilha.size());
		pilha.pop();
		assertEquals(0,pilha.size());
	}

	@Test
	void testIsEmpty() {
		PilhaEncadeada pilha = new PilhaEncadeada();
		assertTrue(pilha.isEmpty());
		pilha.push("teste");
		assertFalse(pilha.isEmpty());
		pilha.pop();
		assertTrue(pilha.isEmpty());
	}

}
