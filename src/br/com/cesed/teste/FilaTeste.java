package br.com.cesed.teste;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.Before;
import org.junit.jupiter.api.Test;

import br.com.cesed.fila.FilaEncadeada;

class FilaTeste {


	@Test
	void testCriarFila() {
		FilaEncadeada fila = new FilaEncadeada();
		assertEquals(0, fila.size());
		fila.criarFila(1);
		assertEquals(1, fila.size());
		fila.dequeue();
		fila.criarFila(2);
		assertEquals(1, fila.size());
	}

	@Test
	void testEnqueue() {
		FilaEncadeada fila = new FilaEncadeada();
		fila.enqueue("claudio");
		assertEquals(1, fila.size());
		fila.enqueue("teste");
		fila.enqueue("1");
		fila.enqueue("2");
		assertEquals(4, fila.size());
		fila.dequeue();
		assertEquals(3, fila.size());
		
		
	}

	@Test
	void testDequeue() {
		FilaEncadeada fila = new FilaEncadeada();
		assertThrows(IllegalArgumentException.class, () ->{
			fila.dequeue();	
		});
		fila.enqueue(1);
		fila.dequeue();
		assertEquals(0, fila.size());
		assertTrue(fila.isEmpty());
	}

	@Test
	void testFront() {
		FilaEncadeada fila = new FilaEncadeada();
		assertEquals(0, fila.size());
		assertTrue(fila.isEmpty());
		fila.enqueue(1);
		assertEquals(1, fila.front());
		fila.enqueue(2);
		assertEquals(1, fila.front());
		fila.dequeue();
		assertEquals(2, fila.front());
		fila.enqueue(10);
		fila.enqueue(11);
		fila.enqueue(49);
		fila.front();
		assertEquals(2, fila.front());
		fila.dequeue();
		fila.dequeue();
		fila.dequeue();
		assertEquals(49, fila.front());
		fila.dequeue();
		assertEquals(0, fila.size());
		assertThrows(NullPointerException.class, ()->{
			fila.front();
		});
		
	}

	@Test
	void testSize() {
		FilaEncadeada fila = new FilaEncadeada();
		assertEquals(0, fila.size());
		fila.enqueue(1);
		assertEquals(1, fila.size());
		fila.dequeue();
	
	}

	@Test
	void testIsEmpty() {
		FilaEncadeada fila = new FilaEncadeada();
		assertTrue(fila.isEmpty());
		fila.criarFila(1);
		assertFalse(fila.isEmpty());
		
	}

}
