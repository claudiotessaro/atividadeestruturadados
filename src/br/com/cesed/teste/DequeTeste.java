package br.com.cesed.teste;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import br.com.cesed.deque.DequeEncadeado;

class DequeTeste {

	@Test
	void testCriarLista() {
		DequeEncadeado deque = new DequeEncadeado();
		assertTrue(deque.isEmpty());
		assertEquals(0, deque.size());
		deque.criarLista("cachorro");
		assertFalse(deque.isEmpty());
		assertEquals(1, deque.size());
	}

	@Test
	void testInsertFirst() {
		DequeEncadeado deque = new DequeEncadeado();
		assertEquals(0, deque.size());
		assertTrue(deque.isEmpty());
		deque.insertFirst("cachorro");
		assertEquals("cachorro", deque.getComeco());
		deque.insertFirst("leao");
		assertEquals("leao", deque.getComeco());
		assertEquals("cachorro", deque.getFinal());
		deque.remove(1);
		assertEquals(1, deque.size());
		deque.remove(0);
		assertEquals(0, deque.size());
		
	}

	@Test
	void testInsertLast() {
		DequeEncadeado deque = new DequeEncadeado();
		assertEquals(0, deque.size());
		assertTrue(deque.isEmpty());
		deque.insertLast("teste");
		assertEquals("teste", deque.getFinal());
		deque.insertLast("girafa");
		assertEquals("girafa",deque.getFinal());
		
	}

	@Test
	void testRemoveFirst() {
		DequeEncadeado deque = new DequeEncadeado();
		assertEquals(0, deque.size());
		assertTrue(deque.isEmpty());
		deque.insertFirst(1);
		deque.insertFirst(2);
		deque.removeFirst();
		assertEquals(1, deque.size());

	}

	@Test
	void testRemoveLast() {
		DequeEncadeado deque = new DequeEncadeado();
		assertEquals(0, deque.size());
		assertTrue(deque.isEmpty());
		deque.insertFirst(1);
		deque.insertFirst(2);
		deque.removeFirst();
		assertEquals(1, deque.size());
	}

	@Test
	void testRemove() {
		DequeEncadeado deque = new DequeEncadeado();
		assertEquals(0, deque.size());
		assertTrue(deque.isEmpty());
		deque.insertFirst(1);
		deque.insertFirst(2);
		deque.remove(1);
		assertEquals(1, deque.size());
		deque.remove(0);
		assertEquals(0, deque.size());
	}

	@Test
	void testSize() {
		DequeEncadeado deque = new DequeEncadeado();
		assertEquals(0, deque.size());
		assertTrue(deque.isEmpty());
		deque.insertFirst(1);
		assertEquals(1,deque.size());
	}

	@Test
	void testIsEmpty() {
		DequeEncadeado deque = new DequeEncadeado();
		assertEquals(0, deque.size());
		assertTrue(deque.isEmpty());
		deque.insertFirst(1);
		assertFalse(deque.isEmpty());
	}

}
