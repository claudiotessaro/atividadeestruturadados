package br.com.cesed.lista;

/***
 * Implementa��o da estrutura de dados Lista Duplamente encadeada
 * 
 * @author claudio tessaro
 *
 */
public class ListaDE {

	private No primeiro;
	private No ultimo;

	int tamanho;

	/***
	 * Metodo que cria uma lista caso n�o exista lista criada
	 * 
	 * @param valor
	 */
	public void criarLista(Object valor) {
		if (primeiro == null || ultimo == null) {
			this.primeiro = new No(valor);
			this.ultimo = new No(valor);
			tamanho++;
		}

	}

	/***
	 * Metodo que adiciona um elemento na lista
	 * 
	 * @param elemento
	 */
	public void addInicio(Object elemento) {
		if (this.primeiro == null) {
			criarLista(elemento);
		} else {
			No novo = new No(elemento);
			primeiro.setAnterior(novo);
			primeiro = novo;
			tamanho++;
		}

	}

	/***
	 * Metodo que adiciona no final da lista
	 * 
	 * @param elemento
	 */
	public void addFinal(Object elemento) {
		if (this.primeiro == null) {
			criarLista(elemento);
		} else {
			No novo = new No(elemento);
			ultimo.setProximo(novo);
			ultimo = novo;
			tamanho++;
		}
	}

	/***
	 * Metodo que remove no inicio da lista
	 */
	public void removeInicio() {
		if (!posicaoOcupada(0)) {
			throw new IllegalArgumentException("Posi��o n�o existe");
		}
		primeiro = primeiro.getProximo();
		tamanho--;
		if (tamanho == 0) {
			ultimo = null;
		}
	}

	/***
	 * Metodo que remove o ultimo elemento da lista
	 */
	public void removeFim() {
		if (!posicaoOcupada(tamanho - 1)) {
			throw new IllegalArgumentException("Posi��o n�o existe");
		}
		if (tamanho == 1) {
			removeInicio();
		} else {
			No penultima = ultimo.getAnterior();
			ultimo = penultima;
			tamanho--;

		}
	}

	/***
	 * Metodo que remove um elemento passando a posicao do elemento pelo @param
	 * 
	 * @param index
	 */
	public void remove(int elemento) {
		No noTemp = primeiro;
		No noAnt = null;
		if (primeiro.getElemento().equals(elemento)) {
			primeiro = primeiro.getProximo();
		} else {
			while (noTemp != null && !noTemp.getElemento().equals(elemento)) {
				noAnt = noTemp;
				noTemp = noTemp.getProximo();
			}
			if (noTemp != null) {
				noAnt.setProximo(noTemp.getProximo());
			}
			if (noTemp == ultimo) {
				ultimo = noAnt;
			}
		}
		tamanho--;
	}

	/***
	 * Metodo responsavel pro pegar o primeiro valor do deque
	 * 
	 * @return primeiro valor inserido
	 */
	public Object getComeco() {
		return primeiro.getElemento();
	}

	/***
	 * Metodo responsavel por pegar o ultimo valor inserido
	 * 
	 * @return ultimo valor
	 */
	public Object getFinal() {
		return ultimo.getElemento();
	}

	/***
	 * Metodo que retorna o tamanho da lista
	 * 
	 * @return tamanho
	 */
	public int size() {
		return tamanho;
	}

	/***
	 * Metodo que retorna se uma lista � vazia ou n�o
	 * 
	 * @return true
	 */
	public boolean isEmpty() {
		return tamanho == 0;
	}


	private boolean posicaoOcupada(int posicao) {
		return posicao >= 0 && posicao < this.tamanho;
	}

	public No getPrimeiro() {
		return primeiro;
	}

	public No getUltimo() {
		return ultimo;
	}

	public void setPrimeiro(No primeiro) {
		this.primeiro = primeiro;
	}

	public void setUltimo(No ultimo) {
		this.ultimo = ultimo;
	}

}
