package br.com.cesed.deque;

import br.com.cesed.lista.No;

/***
 * Implementação da estrutura de dados deque encadeado
 * 
 * @author claudio tessaro
 *
 */
public class DequeEncadeado {

	private No primeiro;
	private No ultimo;

	int tamanho;

	/***
	 * metodo responsavel por criar uma lista que esteja vazia
	 * 
	 * @param elemento
	 */
	public void criarLista(Object elemento) {
		if (isEmpty()) {
			No novo = new No(elemento);
			primeiro = novo;
			ultimo = novo;
			tamanho++;
		}
	}

	/***
	 * Metodo que insere o elemento no comeco do deque
	 * 
	 * @param elemento
	 */
	public void insertFirst(Object elemento) {
		if (isEmpty()) {
			criarLista(elemento);
		} else {
			No novo = new No(elemento);
			primeiro.setAnterior(novo);
			primeiro = novo;
			System.out.println();
			tamanho++;
		}
	}

	/**
	 * Metodo responsavel por inserir um elemento no final do deque
	 * 
	 * @param elemento
	 */
	public void insertLast(Object elemento) {
		if (isEmpty()) {
			criarLista(elemento);
		} else {
			No novo = new No(elemento);
			ultimo.setProximo(novo);
			ultimo = novo;
			tamanho++;
		}
	}

/***
 * Metodo responsavel por remover o primeiro elemento	
 */
	public void removeFirst() {
		remove(0);
	}
/***
 * Metodo responsavel por remover o ultimo elemento
 */
	public void removeLast() {
		remove(tamanho-1);
	}

	/***
	 * Metodo que remove um elemento passando o @param
	 * 
	 * @param index
	 */	
	public void remove(int elemento){
		No noTemp = primeiro;
		No noAnt = null;
		if (primeiro.getElemento().equals(elemento)){							
			primeiro = primeiro.getProximo();
		}else{
			while (noTemp !=null && !noTemp.getElemento().equals(elemento)){
			
				noAnt = noTemp;
				noTemp = noTemp.getProximo();
			}
			if(noTemp != null)
			{
				noAnt.setProximo(noTemp.getProximo());
			}
			if(noTemp == ultimo){
				ultimo = noAnt;
			}
		}
		tamanho--;
	}

	/***
	 * Metodo que mostrará se a posicao ocupada esta dentro do tamanho do deque
	 * 
	 * @param posicao
	 * @return
	 */
	private boolean posicaoOcupada(int posicao) {
		return posicao >= 0 && posicao < this.tamanho;
	}

	/***
	 * Metodo que retorna o tamanho de uma lista
	 * 
	 * @return
	 */
	public int size() {
		return tamanho;
	}

	/***
	 * Metodo que retorna se a lista esta vazia ou nao
	 * 
	 * @return
	 */
	public boolean isEmpty() {
		return tamanho == 0;
	}

/***
 * Metodo responsavel pro pegar o primeiro valor do deque	
 * @return primeiro valor inserido
 */
	public Object getComeco() {
		return primeiro.getElemento();
		}
/***
 * Metodo responsavel por pegar o ultimo valor inserido		
 * @return ultimo valor
 */
	public Object getFinal() {
		return ultimo.getElemento();
	}

	public No getPrimeiro() {
		return primeiro;
	}

	public void setPrimeiro(No primeiro) {
		this.primeiro = primeiro;
	}

	public No getUltimo() {
		return ultimo;
	}

	public void setUltimo(No ultimo) {
		this.ultimo = ultimo;
	}

}
