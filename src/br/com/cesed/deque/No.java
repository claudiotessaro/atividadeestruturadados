package br.com.cesed.deque;

public class No {
	
	private Object valor;
	
	private No proximo;
	
	private No anterior;
	
	
	public No(Object valor) {
		this.valor = valor;
		proximo = null;
		anterior = null;
	}

	public Object getValor() {
		return valor;
	}

	public void setValor(Object valor) {
		this.valor = valor;
	}

	public No getProximo() {
		return proximo;
	}

	public void setProximo(No proximo) {
		this.proximo = proximo;
	}

	public No getAnterior() {
		return anterior;
	}

	public void setAnterior(No anterior) {
		this.anterior = anterior;
	}

	
}
