import br.com.cesed.deque.DequeEncadeado;
import br.com.cesed.fila.FilaEncadeada;
import br.com.cesed.lista.ListaDE;
import br.com.cesed.pilha.PilhaEncadeada;

public class App {

	public static void main(String[] args) {
		/*FilaEncadeada fila = new FilaEncadeada();
		System.out.println(fila.isEmpty());
		fila.enqueue(3);
		System.out.println(fila.front());
		System.out.println(fila.size());
		System.out.println(fila.isEmpty());
		System.out.println(fila.dequeue());
		System.out.println(fila.size());
	*/
		
		ListaDE lista = new ListaDE();
		System.out.println(lista.isEmpty());
		System.out.println(lista.size());
		lista.addInicio(11);
		lista.addInicio(10);
		lista.addInicio(3);
		lista.addInicio(4);
		lista.addFinal(5);
		System.out.println(lista.size());
		lista.removeInicio();
		lista.removeFim();
		System.out.println(lista.size());
		lista.remove(1);
		System.out.println(lista.size());
		lista.remove(2);
		System.out.println(lista.size());
		lista.remove(0);

		ListaDE lista2 = new ListaDE();
		lista2.addInicio(11);
		lista2.addInicio(10);
		lista2.addInicio(3);
		lista2.addInicio(4);
		lista2.addFinal(5);
	
		
		/*PilhaEncadeada pilha = new PilhaEncadeada();
		System.out.println(pilha.isEmpty());
		pilha.push(1);
		pilha.push(2);
		pilha.push(3);
		pilha.push(4);
		System.out.println(pilha.size());
		System.out.println(pilha.pop());
		System.out.println(pilha.size());
		System.out.println(pilha.top());
		System.out.println(pilha.size());
		System.out.println(pilha.top());
		System.out.println(pilha.pop());
		System.out.println(pilha.top());
		System.out.println();
	*/
		
		DequeEncadeado deque = new DequeEncadeado();
		System.out.println("tamanho = "+ deque.size());
		System.out.println(deque.isEmpty());
		deque.insertFirst(10);
		deque.insertFirst(20);
		deque.insertFirst(30);

		System.out.println(deque.getComeco());
		deque.remove(0);
		System.out.println(deque.size());
		deque.remove(0);
		
		System.out.println(deque.size());
		
	
	
	}

}
